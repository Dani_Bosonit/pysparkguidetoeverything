Pyspark es la 8ª maravilla de BigData, pero su instalación es bastante ambigua y confusa
en su inmensa mayoría de casos. Por ello este repositorio te va a ayudar a instalar este
software tan útil, además de que trataré de resolver los problemas que vayan surgiendo para
tener un background en caso de que a futuros desarrolladores sufran los mismos errores que
yo sufrí. Lo primero es la instalación:

1º En la carpeta ./resources hay 3 archivos .exe, hay que instalarlos en este orden:

	- Java jre-8.261
	- Java jdk-8.261
	- Python 3.8

2º Es bastante circunstancial pero puedes usar el requirements.txt que hay para instalar en tu
entorno virtual las bibliotecas que yo utilizo en Python. Puedes omitir este paso pero recomiendo
que al menos instales pip (Primer comando) y crees un entorno virtual para Python.
	
	curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
	pip install -r ./resources/requirements.txt

3º Para este momento ya tendrás Java y Python preparados para usarse (Si has recibido algún error
en los pasos previos, ignora este tutorial y usa el punto 1 de El Santo Grial.pdf), 
pero aún queda lo más importante. Coloca la carpeta spark-3.0.1-bin-hadoop2.7 donde quieras, recomiendo
ponerlo en C:/ o similares. Y sin tocar nada más, añade estas variables de entorno apuntando a estas 
carpetas/archivos.
	
	HADOOP_HOME - C:\loquesea\spark-3.0.1-bin-hadoop2.7\hadoop
	SPARK_HOME - C:\loquesea\spark-3.0.1-bin-hadoop2.7
	JAVA_HOME - C:\Program Files\Java\jdk1.8.0_261
	JRE_HOME - C:\Program Files\Java\jre1.8.0_261

Y después modifica el path añadiendo estas nuevas:

	%JAVA_HOME%\bin
	%HADOOP_HOME%\bin
	C:\loquesea\AppData\Local\Programs\Python\Python38\Scripts\
	C:\loquesea\AppData\Local\Programs\Python\Python38\